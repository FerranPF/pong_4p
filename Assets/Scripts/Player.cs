﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	private Vector3 iniPos;
	public float vely;
	private float move;
	public float maxY;
	private Vector3 actualPosition;

	void Awake(){
		iniPos = transform.position;
	}

	void Update () {
		move = Input.GetAxis ("Vertical");

		transform.Translate (0, move * vely * Time.deltaTime, 0);

		if (transform.position.y >= maxY) {
			actualPosition = new Vector3 (transform.position.x, maxY, transform.position.z);
			transform.position = actualPosition;
		} else if (transform.position.y <= -maxY) {
			actualPosition = new Vector3 (transform.position.x, -maxY, transform.position.z);
			transform.position = actualPosition;
		}
	}
}

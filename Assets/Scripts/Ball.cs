﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour {

	private Vector3 iniPos;
	public Vector2 movement;
	public Vector2 marge;
	public float maxvel;
	private Vector3 actualPosition;

	void Awake(){
		iniPos = transform.position;
	}
		

	void Update () {
		transform.Translate (movement.x * Time.deltaTime, movement.y * Time.deltaTime, 0);
	}

	void OnTriggerEnter(Collider other){
		switch (other.tag) {
		case "Player":
			movement.x *= -1.5f;
			break;
		case "Border":
			movement.y *= -1.5f;
			break;
		case "Goal":
                if(transform.position.x < -14)
                {
                    Debug.Log("Te ha marcado un gol.");
                }
                else
                {
                    Debug.Log("Has marcado un gol.");
                }
                transform.position = iniPos;
			break;
		}

		if (movement.x > maxvel) {
			movement.x = maxvel;
		}
		if (movement.x < -maxvel) {
			movement.x = -maxvel;
		}
		if (movement.y > maxvel) {
			movement.y = maxvel;
		}
		if (movement.y < -maxvel) {
			movement.y = -maxvel;
		}
	}
}

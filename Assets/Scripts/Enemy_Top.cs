﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Top : MonoBehaviour {

	public Transform ballPosition;
	private Vector3 iniPos;
	public float vely;
	public float move;
	public float maxX;
	private Vector3 actualPosition;

	void Awake()
	{
		iniPos = transform.position;
	}

	void Update () {
		if(ballPosition.position.y > -4)
		{
			transform.Translate(0, move * vely * Time.deltaTime, 0);
		}
		else
		{
			move = 0;
		}

		if(ballPosition.position.x > transform.position.x)
		{
			move = 1;
		}
		else
		{
			move = -1;
		}

		if(transform.position.x >= maxX)
		{
			actualPosition = new Vector3(maxX, transform.position.y, transform.position.z);
			transform.position = actualPosition;
		}
		else if(transform.position.x <= -maxX)
		{
			actualPosition = new Vector3(-maxX, transform.position.y, transform.position.z);
			transform.position = actualPosition;
		}

	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour {

    public Transform ballPosition;
    private Vector3 iniPos;
    public float vely;
    public float move;
    public float maxY;
    private Vector3 actualPosition;

    void Awake()
    {
        iniPos = transform.position;
    }

    void Update () {
        if(ballPosition.position.x > 0)
        {
            transform.Translate(0, move * vely * Time.deltaTime, 0);
        }
        else
        {
            move = 0;
        }

        if(ballPosition.position.y > transform.position.y)
        {
            move = 1;
        }
        else
        {
            move = -1;
        }

        if(transform.position.y >= maxY)
        {
            actualPosition = new Vector3(transform.position.x, maxY, transform.position.z);
            transform.position = actualPosition;
        }
        else if(transform.position.y <= -maxY)
        {
            actualPosition = new Vector3(transform.position.x, -maxY, transform.position.z);
            transform.position = actualPosition;
        }
        
    }
}
